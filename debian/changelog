node-node-sass (9.0.0+git20240131.6081731+dfsg-3) unstable; urgency=medium

  * Team upload
  * Extend patch to allow node 22

 -- Jérémy Lal <kapouer@melix.org>  Fri, 13 Dec 2024 10:41:20 +0100

node-node-sass (9.0.0+git20240131.6081731+dfsg-2) unstable; urgency=medium

  * Team upload
  * Set a longer test timeout (60s instead of default 2s)

 -- Jérémy Lal <kapouer@melix.org>  Mon, 25 Nov 2024 22:29:27 +0100

node-node-sass (9.0.0+git20240131.6081731+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream version 9.0.0+git20240131.6081731+dfsg
  * Refresh patches

 -- Yadd <yadd@debian.org>  Sun, 31 Mar 2024 06:51:42 +0400

node-node-sass (7.0.3+git20221109.ee13eb9+dfsg-4) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.2
  * Drop build dependency to libuv1-dev (Closes: #1068064)

 -- Yadd <yadd@debian.org>  Sat, 30 Mar 2024 13:35:38 +0400

node-node-sass (7.0.3+git20221109.ee13eb9+dfsg-3) unstable; urgency=medium

  * Team upload
  * patch: allow more node abi. Needed for t64.

 -- Jérémy Lal <kapouer@melix.org>  Wed, 20 Mar 2024 22:57:47 +0100

node-node-sass (7.0.3+git20221109.ee13eb9+dfsg-2) unstable; urgency=medium

  * Team upload
  * Force rebuild

 -- Yadd <yadd@debian.org>  Tue, 29 Nov 2022 09:37:45 +0100

node-node-sass (7.0.3+git20221109.ee13eb9+dfsg-1) unstable; urgency=medium

  * Team upload

  [ Nilesh Patra ]
  * Remove myself from uploaders

  [ Yadd ]
  * Declare compliance with policy 4.6.1
  * Update version
  * New upstream version 7.0.3+git20221109.ee13eb9+dfsg
  * Refresh patches

 -- Yadd <yadd@debian.org>  Sat, 26 Nov 2022 16:33:44 +0100

node-node-sass (7.0.1+git20220307.e6194b1+dfsg-1) unstable; urgency=medium

  [ Yadd ]
  * Team upload
  * New upstream version 7.0.1+git20220307.e6194b1+dfsg

  [ Jérémy Lal ]
  * Update patch: mark node 18 as supported

 -- Jérémy Lal <kapouer@melix.org>  Tue, 28 Jun 2022 14:37:51 +0200

node-node-sass (7.0.1+git20211229.3bb51da+dfsg-1) unstable; urgency=medium

  * Team upload
  * Fix pretty version and repack

 -- Yadd <yadd@debian.org>  Mon, 17 Jan 2022 13:57:28 +0100

node-node-sass (4.14.1+git20211229.3bb51da+dfsg-1) unstable; urgency=medium

  * Team upload
  * Update nodejs dependency to nodejs:any
  * New upstream version 4.14.1+git20211229.3bb51da+dfsg
  * Refresh patches
  * Update copyright
  * Drop custom "files"
  * Modernize test
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Mon, 17 Jan 2022 13:52:05 +0100

node-node-sass (4.14.1+git20200512.e1fc158+dfsg-6) unstable; urgency=medium

  * Team upload
  * Fix test for mips64el
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Tue, 12 Oct 2021 06:26:36 +0200

node-node-sass (4.14.1+git20200512.e1fc158+dfsg-5) unstable; urgency=medium

  * Team upload

  [ Nilesh Patra ]
  * Update email

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch

  [ Yadd ]
  * Declare compliance with policy 4.6.0
  * Add ctype=nodejs to component(s)
  * Add fix for node-meow ≥ 8
  * Drop unneeded dependency version constraints

 -- Yadd <yadd@debian.org>  Mon, 11 Oct 2021 10:30:22 +0200

node-node-sass (4.14.1+git20200512.e1fc158+dfsg-4) unstable; urgency=medium

  * Team upload
  * Back to unstable after successful tests

 -- Xavier Guimard <yadd@debian.org>  Sat, 24 Oct 2020 19:54:28 +0200

node-node-sass (4.14.1+git20200512.e1fc158+dfsg-3) experimental; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Use dh-sequence-nodejs
  * Add fix for node-mkdirp ≥ 1

 -- Xavier Guimard <yadd@debian.org>  Fri, 23 Oct 2020 16:17:30 +0200

node-node-sass (4.14.1+git20200512.e1fc158+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Tightening dependency on nodejs (Closes: #963761). Thanks Jonas Smedegaard
    and Paul Gevers.

 -- Andrius Merkys <merkys@debian.org>  Wed, 22 Jul 2020 01:12:22 -0400

node-node-sass (4.14.1+git20200512.e1fc158+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Andrius Merkys ]
  * New upstream version 4.14.1+git20200512.e1fc158+dfsg
  * Excluding src/libsass, linking against system-provided libsass
    (Closes: #963764)
  * Ignoring failing test case

  [ Nilesh Patra ]
  * Add compression parameter

 -- Andrius Merkys <merkys@debian.org>  Thu, 09 Jul 2020 08:14:41 -0400

node-node-sass (4.14.1-2) unstable; urgency=medium

  * Team upload
  * Remove dependency to node-cross-spawn (Closes: #959785)

 -- Xavier Guimard <yadd@debian.org>  Tue, 05 May 2020 13:49:49 +0200

node-node-sass (4.14.1-1) unstable; urgency=medium

  * Team upload
  * Add fix for readable-stream ≥ 3 (Closes: #959780)
  * New upstream release
  * Remove useless dependency to node-request (Closes: #958696)

 -- Xavier Guimard <yadd@debian.org>  Tue, 05 May 2020 12:36:55 +0200

node-node-sass (4.13.1-3) unstable; urgency=medium

  * Team upload
  * Add debian/patches/0005-build-on-more-architectures.diff
    as upstream authors had arbitrarily limited to i386 and amd64 only
    when it can build successfully on so many more architectures

 -- Anthony Fok <foka@debian.org>  Wed, 11 Mar 2020 08:27:09 -0600

node-node-sass (4.13.1-2) unstable; urgency=medium

  * Team upload.
  * Change --libsass_ext=auto to --libsass_ext=no temporarily
    to embed own copy of libsass 3.5.4 as a last resort to solve
    a FTBFS conundrum where all other libsass bindings and Debian
    have moved on to libsass 3.6.3, but unfortunately the node-sass
    upstream authors showed no interest in catching up any time soon,
    see https://github.com/sass/node-sass/issues/2685
    .
    Upstream author @xzyfer promises that libsass 3.6 support "will land
    in 5.0", so please remember to revert this commit when that happens.

 -- Anthony Fok <foka@debian.org>  Sun, 08 Mar 2020 09:50:12 -0600

node-node-sass (4.13.1-1) unstable; urgency=medium

  * Team upload

  [ Nilesh ]
  * Disable failing test for Node.js >= 12
  * Add manpage
  * Add "Rules-Requires-Root: no"
  * Bump standards version to 4.5.0
  * Fix lintian

  [ Xavier Guimard ]
  * New upstream version 4.13.1
  * Remove disable_api_test patch, fixed by upgrade

 -- Xavier Guimard <yadd@debian.org>  Tue, 28 Jan 2020 07:20:18 +0100

node-node-sass (4.12.0-2) unstable; urgency=medium

  * Team upload

  [ Xavier Guimard ]
  * Fix link

  [ Pirate Praveen ]
  * Source-only upload for bullseye
  * Bump Standards-Version to 4.4.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sat, 30 Nov 2019 14:12:58 +0530

node-node-sass (4.12.0-1) unstable; urgency=low

  [ Nilesh  Sat, 20 Jul 2019 23:10:19 -0700 ]
  * Initial release (Closes: #932956)

 -- Nilesh <npatra974@gmail.com>  Sat, 20 Jul 2019 23:11:41 -0700
