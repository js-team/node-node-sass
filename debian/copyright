Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: node-sass
Upstream-Contact: https://github.com/sass/node-sass/issues
Source: https://github.com/sass/node-sass
Files-Excluded:
 src/libsass

Files: *
Copyright: 2013-2016, Andrew Nesbitt <andrewnez@gmail.com>
License: Expat

Files: async-foreach/*
Copyright: 2011, "Cowboy" Ben Alman
License: Expat

Files: gaze/*
Copyright: 2018, Kyle Robinson Young
License: Expat

Files: gaze/lib/helper.js
Copyright: 2018, Kyle Robinson Young
 2012-2013, he Dojo Foundation <http://dojofoundation.org/>
 2009-2013, Jeremy Ashkenas, DocumentCloud Inc.
 2010, Caolan McMahon
License: Expat
Comment: Embeds some function copied form lodash, underscore, async

Files: get-stdin/*
Copyright: Sindre Sorhus <sindresorhus@gmail.com>
License: Expat

Files: js-base64/*
Copyright: 2014, Dan Kogai
License: BSD-3-Clause

Files: sass-graph/*
Copyright: xzyfer
License: Expat

Files: sass-spec/*
Copyright: 2007-2014, The Sass Authors
License: Expat

Files: scss-tokenizer/*
Copyright: 2015, sasstools
License: Expat

Files: stdout-stream/*
Copyright: 2013, Mathias Buus
License: Expat

Files: true-case-path/*
Copyright: barsh
 Michael Klement <mklement0@gmail.com>
License: Apache-2.0
Comment: The code for this project was sourced from
 https://stackoverflow.com/a/33139702/45375
 .
 The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: debian/*
Copyright: 2019, Nilesh <npatra974@gmail.com>
 2020, Andrius Merkys <merkys@debian.org>
 2021-2022, Yadd <yadd@debian.org>
License: Expat

Files: debian/tests/test_modules/read-yaml/*
Copyright: 2014-2017, Jon Schlinkert
License: Expat

Files: debian/tests/test_modules/uid2/*
Copyright: 2013, Marco Aurelio
License: Expat

Files: debian/tests/test_modules/unique-temp-dir/*
Copyright: James Talmage <james@talmage.io>
License: Expat

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License 2.0 can
 be found in "/usr/share/common-licenses/Apache-2.0"

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * Neither the name of {{{project}}} nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
